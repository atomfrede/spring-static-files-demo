package com.example.staticfiles.demo;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.resource.PathResourceResolver;

@Configuration
public class MvcConfiguration extends WebMvcConfigurerAdapter {

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        // For examples using Spring 4.1.0
        registry.addResourceHandler("/scorm/**")
                // The trailing slash here is very important
                .addResourceLocations("file:/home/fred/scorm/")
                .setCachePeriod(3600).resourceChain(true)
                .addResolver(new PathResourceResolver());
    }
}
